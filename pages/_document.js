import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="en">
        <Head />
            <title>alonzo-austin</title>
            <meta name="mobile-web-app-capable" content="yes" />
            <meta name="apple-mobile-web-app-capable" content="yes" />
            <meta name="application-name" content="alonzo-austin" />
            <meta name="apple-mobile-web-app-title" content="alonzo-austin" />
            <meta name="theme-color" content="#2f3e46" />
            <meta name="msapplication-navbutton-color" content="#2f3e46" />
            <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
            <meta name="msapplication-starturl" content="/" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            {/* <meta name="Description" content="this is the official website of Alonzo Austin. This displays my projects, what I used to build them, and the development logs" /> */}
            <link rel="icon"  sizes="512x512" href="/alonzoaustinfavicon.png" />
            <link rel="apple-touch-icon"  sizes="512x512" href="/alonzoaustinfavicon.png" />
            <link rel="icon"  sizes="144x144" href="/alonzoaustinfavicon.png" />
            <link rel="apple-touch-icon"  sizes="144x144" href="/alonzoaustinfavicon.png" />
            <link rel="icon"  sizes="192x192" href="/alonzoaustinfavicon.png" />
            <link rel="apple-touch-icon"  sizes="192x192" href="/alonzoaustinfavicon.png" />
            <link rel="preconnect" href="https://fonts.gstatic.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" />
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@700&display=swap" rel="stylesheet" />
            <link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet" /> 
            <noscript>Sorry but you must have javascript to view my website</noscript>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument