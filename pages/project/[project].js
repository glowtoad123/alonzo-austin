import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import firebase from 'firebase'
import 'firebase/firestore'
import hljs from 'highlight.js'
import 'markdown-it'
import MarkdownIt from 'markdown-it'
import styles from '../../styles/project.module.css'
import Head from 'next/head'
import Github from '../../public/github.svg'
import External from '../../public/external.svg'
import Home from '../../public/home.svg'

export default function project({data}) {

    hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));

    const md = new MarkdownIt({
        highlight: function (str, lang) {
          if (lang && hljs.getLanguage(lang)) {
            try {
              return '<pre class="hljs"><code>' +
                     hljs.highlight(lang, str, true).value +
                     '</code></pre>';
            } catch (err) {console.log(err)}
          }
      
          return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
        }
      });

      hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));

    /* const router = useRouter()
    const id = router.query.title
    var projectData = data.filter(project => project.id === id)
    projectData = projectData[0]

    console.log("id", id) */

    var projectData = data
    
    console.log("projectData", projectData)
    projectData && console.log("projectData.data", projectData.data)
    

    return (
        <>
            <Head>
              <title>{projectData && projectData.data.title}</title>
              <meta property="og:title" content={projectData && projectData.data.title} key={projectData && projectData.data.title} />
              <meta name="Description" content={projectData && projectData.data.description.slice(0, 130)} />
            </Head>
            <div id={styles.projectBar}>
                <a className={styles.link} href={projectData && projectData.data.repository} target="_blank" rel="noopener noreferrer">
                    {/* <svg id={styles.github} aria-hidden="true" focusable="false" data-prefix="fab" data-icon="github-alt"
                      class="svg-inline--fa fa-github-alt fa-w-15" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 480 512">
                      <path fill="currentColor"
                        d="M186.1 328.7c0 20.9-10.9 55.1-36.7 55.1s-36.7-34.2-36.7-55.1 10.9-55.1 36.7-55.1 36.7 34.2 36.7 55.1zM480 278.2c0 31.9-3.2 65.7-17.5 95-37.9 76.6-142.1 74.8-216.7 74.8-75.8 0-186.2 2.7-225.6-74.8-14.6-29-20.2-63.1-20.2-95 0-41.9 13.9-81.5 41.5-113.6-5.2-15.8-7.7-32.4-7.7-48.8 0-21.5 4.9-32.3 14.6-51.8 45.3 0 74.3 9 108.8 36 29-6.9 58.8-10 88.7-10 27 0 54.2 2.9 80.4 9.2 34-26.7 63-35.2 107.8-35.2 9.8 19.5 14.6 30.3 14.6 51.8 0 16.4-2.6 32.7-7.7 48.2 27.5 32.4 39 72.3 39 114.2zm-64.3 50.5c0-43.9-26.7-82.6-73.5-82.6-18.9 0-37 3.4-56 6-14.9 2.3-29.8 3.2-45.1 3.2-15.2 0-30.1-.9-45.1-3.2-18.7-2.6-37-6-56-6-46.8 0-73.5 38.7-73.5 82.6 0 87.8 80.4 101.3 150.4 101.3h48.2c70.3 0 150.6-13.4 150.6-101.3zm-82.6-55.1c-25.8 0-36.7 34.2-36.7 55.1s10.9 55.1 36.7 55.1 36.7-34.2 36.7-55.1-10.9-55.1-36.7-55.1z">
                      </path>
                    </svg> */}
                    <Github id={styles.github} />
                </a>
                <Link href="/">
                  <a className={styles.link}>
                    {/* <svg id={styles.home} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M8 3.293l6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                      <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                    </svg> */}
                    <Home />
                  </a>
                </Link>
                <a className={styles.link} href={projectData && projectData.data.url} target="_blank" rel="noopener noreferrer">
                    {/* <svg id={styles.url} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="external-link-square-alt"
                      class="svg-inline--fa fa-external-link-square-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 448 512">
                      <path fill="currentColor"
                        d="M448 80v352c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48zm-88 16H248.029c-21.313 0-32.08 25.861-16.971 40.971l31.984 31.987L67.515 364.485c-4.686 4.686-4.686 12.284 0 16.971l31.029 31.029c4.687 4.686 12.285 4.686 16.971 0l195.526-195.526 31.988 31.991C358.058 263.977 384 253.425 384 231.979V120c0-13.255-10.745-24-24-24z">
                      </path>
                    </svg> */}
                    <External />
                </a>
            </div>

            <div id={styles.project}>
              <h1 id={styles.title}>{projectData && projectData.data.title}</h1>

              <br />

              <p id={styles.description}>{projectData && projectData.data.description}</p>

              <br />

              <div id={styles.imageList}>
                  {projectData && projectData.data.images && projectData.data.images.length > 0 && projectData.data.images.map(image => 
                      <img alt={image} className={styles.gallery} width="400" src={image} />
                  )}
              </div> 
              {projectData && <div id={styles.log} dangerouslySetInnerHTML={{__html: md.render(projectData.data.Log, 'javascript')}}></div>}
              
              <br />
            </div>

            <br />
        </>
    )
}


export async function getStaticPaths(){

  !firebase.apps.length && firebase.initializeApp({
    apiKey: process.env.NEXT_APP_FIREBASE_API_KEY,
    authDomain: process.env.NEXT_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.NEXT_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.NEXT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.NEXT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.NEXT_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.NEXT_APP_FIREBASE_APP_ID,
    measurementId: process.env.NEXT_APP_MEASUREMENT_ID
})

const db = firebase.firestore()

var data = []

await db.collection("projects").get().then(project => project.forEach(doc => data.push({id: doc.id, data: doc.data()})))

data.map(project => console.log({params: {project: project.id}}))

console.log("the data on paths", data)

    return {
      paths: data.map(project => ({params: {project: project.id}})),
      fallback: true
    }

}


export async function getStaticProps(context){

    !firebase.apps.length && firebase.initializeApp({
        apiKey: process.env.NEXT_APP_FIREBASE_API_KEY,
        authDomain: process.env.NEXT_APP_FIREBASE_AUTH_DOMAIN,
        databaseURL: process.env.NEXT_APP_FIREBASE_DATABASE_URL,
        projectId: process.env.NEXT_APP_FIREBASE_PROJECT_ID,
        storageBucket: process.env.NEXT_APP_FIREBASE_STORAGE_BUCKET,
        messagingSenderId: process.env.NEXT_APP_FIREBASE_MESSAGING_SENDER_ID,
        appId: process.env.NEXT_APP_FIREBASE_APP_ID,
        measurementId: process.env.NEXT_APP_MEASUREMENT_ID
    })
  
    const db = firebase.firestore()
  
/*     var data = []
  
    await db.collection("blogs").get().then(blog => blog.forEach(doc => data.push({id: doc.id, data: doc.data()}))) */

    var data = {}

    await db.collection("projects").doc(context.params.project).get().then(project=> {data.id = project.id, data.data = project.data()})
  
    console.log("the data", data)
  
    return {
  
        props: {
            data: JSON.parse(JSON.stringify(data)),
        },

        revalidate: 1,
    }
  }