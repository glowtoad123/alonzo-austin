import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import { useRouter } from 'next/router'
import firebase from 'firebase'
import 'firebase/firestore'
import hljs from 'highlight.js'
import 'markdown-it'
import MarkdownIt from 'markdown-it'
import styles from '../styles/project.module.css'

export default function blog({data}) {

    hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));

    const md = new MarkdownIt({
        highlight: function (str, lang) {
          if (lang && hljs.getLanguage(lang)) {
            try {
              return '<pre class="hljs"><code>' +
                     hljs.highlight(lang, str, true).value +
                     '</code></pre>';
            } catch (err) {console.log(err)}
          }
      
          return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
        }
      });

      hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));

    const router = useRouter()
    const id = router.query.title
    var blogData = data.filter(blog => blog.id === id)
    blogData = blogData[0]

    console.log("id", id)
    
    console.log("blogData", blogData)
    blogData && console.log("blogData.data", blogData.data)
    

    return (
        <>
            <Head>
              <title>{blogData && blogData.data.title}</title>
              <meta property="og:title" content={blogData && blogData.data.title} key={blogData && blogData.data.title} />
              <meta name="Description" content={blogData && blogData.data.content.slice(0, 130)} />
            </Head>
            <div id={styles.projectBar}>
                <Link href="/">
                  <a className={styles.link}>
                    <svg style={{marginTop: "0"}} id={styles.home} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M8 3.293l6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                      <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                    </svg>
                  </a>
                </Link>
            </div>

            <div id={styles.article}>
              <h1 id={styles.blogTitle}>{blogData && blogData.data.title}</h1>

              <br />
              <h6 className={styles.blogCardDate}>{blogData && new Date(blogData.data.date.seconds * 1000).toString().split(" ").slice(0, 4).join(" ")}</h6>
              <br />
              {blogData && <div id={styles.log} dangerouslySetInnerHTML={{__html: md.render(blogData.data.content, 'javascript')}}></div>}
              
              <br />
            </div>

            <br />
        </>
    )
}


export async function getStaticProps(context){

    !firebase.apps.length && firebase.initializeApp({
        apiKey: process.env.NEXT_APP_FIREBASE_API_KEY,
        authDomain: process.env.NEXT_APP_FIREBASE_AUTH_DOMAIN,
        databaseURL: process.env.NEXT_APP_FIREBASE_DATABASE_URL,
        projectId: process.env.NEXT_APP_FIREBASE_PROJECT_ID,
        storageBucket: process.env.NEXT_APP_FIREBASE_STORAGE_BUCKET,
        messagingSenderId: process.env.NEXT_APP_FIREBASE_MESSAGING_SENDER_ID,
        appId: process.env.NEXT_APP_FIREBASE_APP_ID,
        measurementId: process.env.NEXT_APP_MEASUREMENT_ID
    })
  
    const db = firebase.firestore()
  
    var data = []
  
    await db.collection("blogs").get().then(blog => blog.forEach(doc => data.push({id: doc.id, data: doc.data()})))
  
    console.log("the data", data)
  
    return {
  
        props: {
            data: JSON.parse(JSON.stringify(data)),
        },

        revalidate: 1,
    }
  }