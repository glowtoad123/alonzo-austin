import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import Card from '../components/card'
import firebase from 'firebase'
import 'firebase/firestore'
import Blogcard from '../components/blogcard'
import { useEffect, useState } from 'react'
import Projects from '../public/projects.svg'
import Blog from '../public/blog.svg'

export default function Home({data, blogData, skills}) {
  const [iosCheck, setIosCheck] = useState(false)
  /* console.log("data", data) */

  console.log("blogData", new Date(blogData[0].data.date.seconds * 1000))

  var blogs = blogData

  blogs.map(blog => blog.data.date.creation = new Date(blog.data.date.seconds * 1000))
  /* console.log("blogs", blogs) */

/*   useEffect(() => {
    if (navigator.platform === "iPhone" || navigator.platform === "iPad" || navigator.platform.slice(0, 5) === "mac") {
      setIosCheck(true)
    }
    console.log("navigator.platform", navigator.platform)
  }, []) */

  console.log("iosCheck", iosCheck)

  console.log("renderedSKills", skills)


  return (
    <>
      <Head>
        <title>Alonzo Austin's Official Website</title>
        {/* <meta name="msvalidate.01" content="B913091A1850ED2E837CF410DA656F3D" /> */}
        <meta name="Description" content="this is the official website of Alonzo Austin. This displays my projects, what I used to build them, and the development logs" />
        <meta name="keywords" content="Alonzo Austin, alonzoaustin, alonzoaustin.com, Alonzoaustin.com, NextJS, nextJS, Nextjs, nextjs, ReactJS, reactJS, Reactjs, reactjs, FaunaDB, faunaDB, Faunadb, faunadb, HTML, CSS, JavaScript,"></meta>
        <link rel="icon" href="/alonzoaustinfavicon.png" />
      </Head>
        <div className={styles.infoBar}>
          <a className={styles.link} href="#project">
            <div className={styles.infoBarDiv}>
              <Projects />
              <label>Projects</label>
            </div>
          </a>
          <a className={styles.link} href="#blog">
            <div className={styles.infoBarDiv}>
              <Blog />
              <label>Blog</label>
            </div>
          </a>
          <a className={styles.infoBarDiv} href="https://github.com/glowtoad123" target="_blank" rel="noopener noreferrer">
            <img alt="Alonzo Austin's github page" src="/github.svg" className={styles.infoBarImage} />
            <label>Github</label>
          </a>
          <a className={styles.infoBarDiv} href="https://www.linkedin.com/in/alonzo-austin/" target="_blank" rel="noopener noreferrer">
            <img alt="Alonzo Austin's Linkedin page" src="/linkedin.svg" className={styles.infoBarImage} />
            <label>Linkedin</label>
          </a>
        </div>
        <div className={styles.myContainer}>
          <img className={styles.myPic} alt="Alonzo Austin" src="/mypic.png" />
          {iosCheck === false ?
          <div className={styles.myInfo}>
            <h1>Alonzo Austin</h1>
            <h3>Fullstack Web Developer</h3>
            <h3>(727) 687-0625</h3>
            <h3>alonzo6546@outlook.com</h3>
          </div>
          :
          <div className={styles.myInfoApple}>
           <h1>Alonzo Austin</h1>
           <h3>Fullstack Web Developer</h3>
           <h3>(727) 687-0625</h3>
           <h3>alonzo6546@outlook.com</h3>
          </div>
          }
        </div>
        {/* <p style={{fontSize: "small"}}>Hello, I am looking for a new opportunity to help develop/maintain/debug your next web app</p> */}



        <div id="project" className={styles.sectionDiv}>
          <h2 className={styles.sectionLabel}>Projects</h2>
        </div>

        <hr />

        <br />
        {/* <div className={styles.cardContainer}> */}
          {data.map(project => 
            <Card
              id={project.id}
              title={project.data.title}
              showcaseImage={project.data.showcaseImage}
              description={project.data.description}
              categories={project.data.categories}
            />
          )}
        {/* </div> */}
        <br />

        <div id="blog" className={styles.sectionDiv}>
          <h2 className={styles.sectionLabel}>Blog</h2>
        </div> 
        
        <hr />
        <br />
        <div>
          {blogs.map(blog => 
            <Blogcard
              id={blog.id}
              title={blog.data.title}
              date={blog.data.date.creation.toString().split(" ").slice(0, 4).join(" ")}
              description={blog.data.content}
            />
          )}
        </div>
        <br />
        <div className={styles.skillContainer}>
            {skills.map(skillList => skillList.map(skill => <p className={styles.skill}>{skill}</p>))}
        </div>
    </>
  )
}


export async function getStaticProps(){

  !firebase.apps.length && firebase.initializeApp({
      apiKey: process.env.NEXT_APP_FIREBASE_API_KEY,
      authDomain: process.env.NEXT_APP_FIREBASE_AUTH_DOMAIN,
      databaseURL: process.env.NEXT_APP_FIREBASE_DATABASE_URL,
      projectId: process.env.NEXT_APP_FIREBASE_PROJECT_ID,
      storageBucket: process.env.NEXT_APP_FIREBASE_STORAGE_BUCKET,
      messagingSenderId: process.env.NEXT_APP_FIREBASE_MESSAGING_SENDER_ID,
      appId: process.env.NEXT_APP_FIREBASE_APP_ID,
      measurementId: process.env.NEXT_APP_MEASUREMENT_ID
  })

  const db = firebase.firestore()

  var data = []
  var blogData = []
  var skills = []

  await db.collection("projects").orderBy("date", "desc").get().then(project => project.forEach(doc => data.push({id: doc.id, data: doc.data()})))
  await db.collection("blogs").orderBy("date", "desc").get().then(blog => blog.forEach(doc => blogData.push({id: doc.id, data: doc.data()})))
  await db.collection("skills").doc("skills").get().then(skill => skills.push(skill.data()))

  skills = skills.map(inside => inside && inside.skills.map(everyOne => everyOne))
  skills = skills.map(inside => inside && inside.map(one => one && one))

  
  console.log("the data", data)
  
  console.log("skills", skills)

  return {

      props: {
          data: JSON.parse(JSON.stringify(data)),
          blogData: JSON.parse(JSON.stringify(blogData)),
          skills: skills
      },

      revalidate: 1,
  }
}