/* module.exports = {
    images: {
      domains: ['firebasestorage.googleapis.com'],
      imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
    },
  } */

  module.exports = {
    webpack(config) {
      config.module.rules.push({
        test: /\.svg$/,
        use: ["@svgr/webpack"]
      });
  
      return config;
    }
  };