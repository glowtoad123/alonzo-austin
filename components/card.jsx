import React from 'react'
import Link from 'next/link'
import styles from '../styles/card.module.css'

export default function Card(props) {
    return (
        <>
        <div className={styles.card}>
            <div className={styles.cardInfo}>
                <Link href={`/project/${props.id}`}>
                    <div className={styles.title}>
                        <p>{props.title}</p>
                    </div>
                </Link>
                <br />
                <div className={styles.descriptionDiv}>
                    <p>{props.description}</p>
                </div>
                <br />
                <div className={styles.categoryContainer}>
                    {props.categories && props.categories.map(category => <p className={styles.category}>{category}</p>)}
                </div>
            </div>
            <Link href={{pathname: "/project", query: {title: props.id},}}>
                <img alt={props.showcaseImage} className={styles.favicon} src={props.showcaseImage} />
            </Link>
        </div>
        
        </>
    )
}
