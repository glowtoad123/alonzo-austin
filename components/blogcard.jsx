import React from 'react';
import MarkdownIt from 'markdown-it'
import hljs from 'highlight.js'
import styles from '../styles/card.module.css'
import Link from 'next/link'

export default function Blogcard(props) {


    hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));

    const md = new MarkdownIt({
        highlight: function (str, lang) {
          if (lang && hljs.getLanguage(lang)) {
            try {
              return '<pre class="hljs"><code>' +
                     hljs.highlight(lang, str, true).value +
                     '</code></pre>';
            } catch (err) {console.log(err)}
          }
      
          return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
        }
      });

      


    return (
        <div className={styles.blogCard}>
                <div className={styles.blogCardInfo}>
                    <Link href={`/blog/${props.id}`}>
                      <div className={styles.blogTitle}>
                          <strong>{props.title}</strong>
                      </div>
                    </Link>
                    <h6 className={styles.blogCardDate}>{props.date}</h6>
                    <div className={styles.content} dangerouslySetInnerHTML={{__html: md.render(props.description)}}></div>
                </div>
        </div>
  )
}
