import React from 'react'
import firebase from 'firebase/app'
import 'firebase/firestore'

export default function fire() {

    const db = firebase.firestore()

    const data = db.collection("projects")

    console.log("data", data)

    return (
        <div>
            testing fire.js
        </div>
    )
}

function getInitialProps(context){

    const app = firebase.initializeApp({
        apiKey: process.env.NEXT_APP_FIREBASE_API_KEY,
        authDomain: process.env.NEXT_APP_FIREBASE_AUTH_DOMAIN,
        databaseURL: process.env.NEXT_APP_FIREBASE_DATABASE_URL,
        projectId: process.env.NEXT_APP_FIREBASE_PROJECT_ID,
        storageBucket: process.env.NEXT_APP_FIREBASE_STORAGE_BUCKET,
        messagingSenderId: process.env.NEXT_APP_FIREBASE_MESSAGING_SENDER_ID,
        appId: process.env.NEXT_APP_FIREBASE_APP_ID,
        measurementId: process.env.NEXT_APP_MEASUREMENT_ID
    })

    return {

        props: {
            app: app
        }
    }
}
